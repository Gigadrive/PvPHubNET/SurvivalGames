package net.pvp_hub.sg;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.GameUtilities;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.sg.chest.ChestManager;
import net.pvp_hub.sg.cmd.Sg;
import net.pvp_hub.sg.cmd.Stats;
import net.pvp_hub.sg.listener.AsyncPlayerChatListener;
import net.pvp_hub.sg.listener.BlockBreakListener;
import net.pvp_hub.sg.listener.BlockPlaceListener;
import net.pvp_hub.sg.listener.CraftItemListener;
import net.pvp_hub.sg.listener.EntityDamageByEntityListener;
import net.pvp_hub.sg.listener.EntityDamageListener;
import net.pvp_hub.sg.listener.InventoryClickListener;
import net.pvp_hub.sg.listener.PlayerChangeFoodLevelListener;
import net.pvp_hub.sg.listener.PlayerDeathListener;
import net.pvp_hub.sg.listener.PlayerInteractListener;
import net.pvp_hub.sg.listener.PlayerJoinListener;
import net.pvp_hub.sg.listener.PlayerLoginListener;
import net.pvp_hub.sg.listener.PlayerMoveListener;
import net.pvp_hub.sg.listener.PlayerPickupItemListener;
import net.pvp_hub.sg.listener.PlayerQuitListener;
import net.pvp_hub.sg.map.AvailableMaps;
import net.pvp_hub.sg.map.Map;
import net.pvp_hub.sg.map.MapVote;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.google.common.collect.Maps;

import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.QueryResult;
import tk.theakio.sql.SQLHandler;

public class SurvivalGames extends JavaPlugin implements Listener {
	
	public static String prefix = "�4[�cSurvivalGames�4] �r�6";
	public String gamestate = "�a�lLobby";
	public ArrayList<Player> spectators = new ArrayList<Player>();
	public ArrayList<Player> online = new ArrayList<Player>();
	public HashMap<Location, Inventory> sgchest = new HashMap<Location, Inventory>();
	public static HashMap<Player, Location> spawns = new HashMap<Player, Location>();
	public File file = new File("plugins/SurvivalGames", "config.yml");
	public FileConfiguration cfg = YamlConfiguration.loadConfiguration(this.file);
	
	private MapVote mv;
	
	private QueryResult qr = null;
	public String main = "main";
	
	private static Map map;
	
	public boolean joinable;
	public boolean onspawn;
	public static boolean friendly;
	public static boolean firstblood;
	public static boolean l;
	
	public int expid;
	public int exp;
	
	public static SurvivalGames instance;
	
	public String mapWorldName = null;
	public String mapBuilder = null;
	public String mapName = null;
	public String mapLink = null;
	
	public ScoreboardManager manager = Bukkit.getScoreboardManager();
	public Scoreboard board = manager.getNewScoreboard();
	public Objective obj = board.registerNewObjective("obj", "dummy");
	
	@SuppressWarnings("deprecation")
	public Score mapOverding = obj.getScore("�e�lMap:");
	@SuppressWarnings("deprecation")
	//public Score map = obj.getScore(cfg.getString("Map.mapName"));
	   
	public SurvivalGames() {
	   instance = this;
	}
	
	public static SurvivalGames getInstance() {
	   return instance;
	}
	
	public int startid;
	public int start;
	public int peace;
	public int peaceid;
	
	public int u = 0;
	
	public int dings = 61;
	
	public int dings2 = 21;
	
	public static int dings3 = 31;
	
	public static int dings4 = 121;
	
	public MapVote getMapVote(){
		return mv;
	}

	public void onEnable(){
		mv = new MapVote(this);
		
		System.out.println("[SurvivalGames] Enabling..");
		System.out.println("[SurvivalGames] ENABLED!");
		
		this.joinable = true;
		this.friendly = true;
		this.onspawn = false;
		this.firstblood = false;
		this.l = false;
		
		this.exp = 120;
		this.start = 15;
		this.peace = 20;
		
		mapWorldName = cfg.getString("Map.worldName");
		mapBuilder = cfg.getString("Map.mapBuilder");
		mapName = cfg.getString("Map.mapName");
		mapLink = cfg.getString("Map.mapLink");
		
		mapOverding.setScore(2);
		//map.setScore(1);
		obj.setDisplayName("�c�l�oPvP-Hub.net �r�8| �4SG");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new ChestManager(this), this);
		Bukkit.getPluginManager().registerEvents(new AsyncPlayerChatListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockBreakListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockPlaceListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamageByEntityListener(this), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamageListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerChangeFoodLevelListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerPickupItemListener(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(this), this);
		Bukkit.getPluginManager().registerEvents(new CraftItemListener(this), this);
		Bukkit.getPluginManager().registerEvents(new InventoryClickListener(this), this);
		//Bukkit.getPluginManager().registerEvents(new ServerPingListener(this), this);
		
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		// /sg
		getCommand("sg").setExecutor(new Sg(this));
		
		// /stats
		getCommand("stats").setExecutor(new Stats(this));
		
		PvPHubCore.setGameState(GameState.LOBBY);
		
		for(World world : Bukkit.getWorlds()){
			world.setAutoSave(false);
		}
		
		SQLHandler Handler = HandlerStorage.addHandler(main, new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API von Akio Zeugs] Putt Putt!");
		}
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			@SuppressWarnings("deprecation")
			public void run(){
				dings--;
				for(Player all : Bukkit.getOnlinePlayers()){
					all.setExp((float) ((double) dings / 60D));
					all.setLevel(dings);
				}
				if(dings == 60){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 50){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 40){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 30){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 20){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 10){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 9){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 8){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 7){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 6){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 5){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 4){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
				} else if(dings == 3){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(dings == 2){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunden");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(dings == 1){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings + " �6Sekunde");
					for(Player all : Bukkit.getOnlinePlayers()){
						all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 1.0F);
					}
				} else if(dings == 0){
					if(Bukkit.getOnlinePlayers().length < 2){
						dings = 61;
						Bukkit.broadcastMessage(prefix + "�cZu wenig Spieler online..");
						for(Player all : Bukkit.getOnlinePlayers()){
							all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 0.5F);
						}
					} else {
						Bukkit.broadcastMessage(prefix + "Das Warmup beginnt!");
						PvPHubCore.setGameState(GameState.WARMUP);
						for (Player all : Bukkit.getOnlinePlayers()){
							all.playSound(all.getEyeLocation(), Sound.NOTE_BASS, 1.0F, 2.0F);
							all.setLevel(0);
							all.setExp(0);
						}
						System.out.println("[MAP] " + mv.getWorldWithMostVotes());
						Map m = new Map(SurvivalGames.getInstance(), mv.getWorldWithMostVotes());
						onspawn = true;
						joinable = false;
						initializer(m);
						
						warmupScheduler();
					}
				}
			}
		}, 20L, 20L);
	}
	
	public void onDisable(){
		for(World world : Bukkit.getWorlds()) {
			Bukkit.getServer().unloadWorld(world.getName(), true);
		}
	}
	
	@EventHandler
	public void onExplode1(EntityExplodeEvent e){
		e.setCancelled(true);
	}
	
	private void initializer(Map map){
		if(map == null){
			System.err.println("ERROR INITIALIZING MAP!!");
			return;
		}
		
		for(Entity e : map.getWorld().getEntities()){
			if(!(e instanceof Player)){
				e.remove();
			}
		}
		
		map.getWorld().setAutoSave(false);
		map.getWorld().setSpawnFlags(false, false);
		map.getWorld().setDifficulty(Difficulty.EASY);
		map.getWorld().setTime(0);
		map.getWorld().setStorm(false);
		map.getWorld().setThundering(false);
		map.getWorld().setGameRuleValue("doMobSpawning", "false");
		map.getWorld().setGameRuleValue("doFireTick", "false");
		map.getWorld().setGameRuleValue("mobGriefing", "false");
		
		for(Player p : getServer().getOnlinePlayers()){
			p.setExp(0);
			p.setLevel(0);
			p.setHealth(((Damageable)p).getMaxHealth());
			teleportOnMap(p, u, map);
			p.getInventory().clear();
		}
		
		String builder = AvailableMaps.fromID(mv.getWorldWithMostVotes()).getBuilder();
		String name = AvailableMaps.fromID(mv.getWorldWithMostVotes()).getName();
		String link = AvailableMaps.fromID(mv.getWorldWithMostVotes()).getLink();
		GameUtilities.sendMapInfos(builder, name, link);
	}
	
	public void teleportOnMap(Player p, int yl, Map map){
		/*World w = Bukkit.getWorld(cfg.getString("SG.Spawn." + yl + ".World"));
		double x = cfg.getDouble("SG.Spawn." + yl + ".x");
		double y = cfg.getDouble("SG.Spawn." + yl + ".y");
		double z = cfg.getDouble("SG.Spawn." + yl + ".z");
		float yaw = cfg.getInt("SG.Spawn." + yl + ".yaw");
		float pitch = cfg.getInt("SG.Spawn." + yl + ".pitch");
		
		Location loc = new Location(w, x, y, z);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		
		spawns.put(p, loc);
		
		p.teleport(loc);*/
		if(map == null) System.out.println("MAP OBJECT IS NULL");
		
		Location loc = map.getSpawnLocation(yl);
		if(AvailableMaps.fromID(mv.getWorldWithMostVotes()) == AvailableMaps.RISE_ORIENT){
			loc.setWorld(Bukkit.getWorld("hg_orient"));
		}
		
		if(AvailableMaps.fromID(mv.getWorldWithMostVotes()) == AvailableMaps.SG4){
			loc.setWorld(Bukkit.getWorld("sg4"));
		}
		
		if(AvailableMaps.fromID(mv.getWorldWithMostVotes()) == AvailableMaps.VENEZIA){
			loc.setWorld(Bukkit.getWorld("dw_venezia"));
		}
		
		p.teleport(map.getSpawnLocation(yl));
		this.spawns.put(p, map.getSpawnLocation(u));
		u++;
	}
	
	public static int getPoints(String p) throws Exception {
		int points = 100;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			points = qr.rs.getInt("points");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return points;
	}
	
	public static int getKills(String p) throws Exception {
		int kills = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			kills = qr.rs.getInt("kills");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return kills;
	}
	
	public static int getDeaths(String p) throws Exception {
		int deaths = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			deaths = qr.rs.getInt("deaths");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return deaths;
	}
	
	public static int getPlayedGames(String p) throws Exception {
		int pgames = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			pgames = qr.rs.getInt("played_games");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return pgames;
	}
	
	public static int getVictories(String p) throws Exception {
		int victories = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			victories = qr.rs.getInt("victories");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return victories;
	}
	
	public static int getShowdowns(String p) throws Exception {
		int showdowns = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			showdowns = qr.rs.getInt("showdowns");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return showdowns;
	}
	
	public static void addDeaths(String p) throws Exception {
		int deaths = SurvivalGames.getDeaths(p);
		
		deaths++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `deaths` = " + deaths + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addKills(String p) throws Exception {
		int kills = SurvivalGames.getKills(p);
		
		kills++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `kills` = " + kills + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addPoints(String p) throws Exception {
		int points = SurvivalGames.getPoints(p);
		
		points++; // +1
		points++; // +2
		points++; // +3
		points++; // +4
		points++; // +5
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `points` = " + points + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addWinPoints(String p) throws Exception {
		int points = SurvivalGames.getPoints(p);
		
		int newpoints = points + 50;
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `points` = " + newpoints + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addVictory(String p) throws Exception {
		int victory = SurvivalGames.getVictories(p);
		
		victory++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `victories` = " + victory + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addShowdown(String p) throws Exception {
		int showdown = SurvivalGames.getShowdowns(p);
		
		showdown++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `showdowns` = " + showdown + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void addPlayedGames(String p) throws Exception {
		int pgames = SurvivalGames.getPlayedGames(p);
		
		pgames++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `played_games` = " + pgames + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public static void removePoints(String p) throws Exception {
		int points = SurvivalGames.getPoints(p);
		
		points--; // -1
		points--; // -2
		points--; // -3
		points--; // -4
		points--; // -5
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `points` = " + points + " WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public void syncData(Player p) throws Exception {
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `uuid` = '" + PlayerUtilities.getUUID(p.getName()) + "' WHERE username='" + p.getName() + "'");
			HandlerStorage.getHandler("main").execute("UPDATE `sg_stats` SET `username` = '" + p.getName() + "' WHERE uuid='" + PlayerUtilities.getUUID(p.getName()) + "'");
			System.out.println("[SG] Data sync succeded! Parameters: " + p.getName());
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public void isInDatabase(String p) throws Exception {
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM sg_stats WHERE uuid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();

		if (qr.rs.getRow() == 0)
	      {
	        System.err.println("User not Found!");
	        HandlerStorage.getHandler("main").execute("INSERT INTO `sg_stats` (`id` ,`uuid` ,`username` ,`kills` ,`deaths` ,`points`)VALUES (NULL , '" + PlayerUtilities.getUUID(p) + "', '" + p + "', '0', '0', '100');");
	      }
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
	}
	
	public void preShowdownScheduler(){
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
			public void run(){
				showdownScheduler();
			}
		}, 1200L);
	}
	
	public void warmupScheduler(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				dings2--;
				if(dings2 == 20){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 10){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 9){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 8){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 7){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 6){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 5){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 4){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 3){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 2){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunden");
				} else if(dings2 == 1){
					Bukkit.broadcastMessage(prefix + "Noch �b" + dings2 + " �6Sekunde");
				} else if(dings2 == 0){
					
					online.clear();
					for(Player all : Bukkit.getOnlinePlayers()){
						online.add(all);
						all.playSound(all.getEyeLocation(), Sound.WITHER_SPAWN, 1.0F, 1.0F);
					}
					
					onspawn = false;
					
					gracePeriod();
					
					gamestate = "�4�lIngame";
				} 
			}
			
		}, 20L, 20L);
	}
	
	public void gracePeriod(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				SurvivalGames.dings3--;
				if(SurvivalGames.dings3 == 30){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 20){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 10){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 9){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 8){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 7){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 6){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 5){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 4){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 3){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 2){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 1){
					Bukkit.broadcastMessage(prefix + "Die Schutzzeit h�lt noch �b" + dings3 + " �6Sekunden");
				} else if(SurvivalGames.dings3 == 0){
					Bukkit.broadcastMessage(prefix + "Das Spiel beginnt!");
					Bukkit.broadcastMessage(prefix + "Sammle Kisten und �berlebe!");
					
					SurvivalGames.friendly = false;
					
					for(Player all : Bukkit.getOnlinePlayers()){
						try {
							SurvivalGames.addPlayedGames(all.getName());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					preShowdownScheduler();
				} 
			}
			
		}, 20L, 20L);
	}
	
	public void showdownScheduler(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				SurvivalGames.dings4--;
				if(SurvivalGames.dings4 == 30){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 20){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 10){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 9){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 8){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 7){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 6){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 5){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 4){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 3){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 2){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunden beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 1){
					Bukkit.broadcastMessage(prefix + "In �b" + dings4 + " �6Sekunde beginnt der Showdown.");
				} else if(SurvivalGames.dings4 == 0){
					
					Bukkit.broadcastMessage(prefix + "�4�lDER SHOWDOWN BEGINNT!");
					
					for(final Player all : Bukkit.getOnlinePlayers()){
						try {
							SurvivalGames.addShowdown(all.getName());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							PlayerUtilities.achieve(20, all);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						Location loc = new Location(Bukkit.getWorld("Lobby"), -1710, 71, -184);
						all.teleport(loc);
						
						all.sendMessage("�e[MAP] Name: �bShowdown");
						all.sendMessage("�e[MAP] Von: �bZeryther");
						all.sendMessage("�e[MAP] Link: �bhttp://zeryther.tk");
					}
				} 
			}
			
		}, 20L, 20L);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	  public void onDestroyByEntity(HangingBreakByEntityEvent event)
	  {
	    String msg1 = PluginMeta.prefix + "Keine Rechte";
	    if ((event.getRemover() instanceof Player))
	    {
	      Player p = (Player)event.getRemover();
	      if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
	        (!p.isOp()) && (!p.hasPermission("frame.remove")))
	      {
	        p.sendMessage(ChatColor.DARK_RED + msg1);
	        event.setCancelled(true);
	        return;
	      }
	    }
	  }
	  
	  @EventHandler(priority=EventPriority.HIGHEST)
	  public void OnPlaceByEntity(HangingPlaceEvent event)
	  {
	    String msg2 = PluginMeta.prefix + "Keine Rechte";
	    Player p = event.getPlayer();
	    if ((event.getEntity().getType() == EntityType.ITEM_FRAME) && 
	      (!p.isOp()) && (!p.hasPermission("frame.place")))
	    {
	      p.sendMessage(ChatColor.DARK_RED + msg2);
	      event.setCancelled(true);
	      return;
	    }
	  }
	  
	  @EventHandler
	  public void canRotate(PlayerInteractEntityEvent event)
	  {
	    String msg3 = PluginMeta.prefix + "Keine Rechte";
	    Entity entity = event.getRightClicked();
	    Player player = event.getPlayer();
	    if (!entity.getType().equals(EntityType.ITEM_FRAME)) {
	      return;
	    }
	    ItemFrame iFrame = (ItemFrame)entity;
	    if ((iFrame.getItem().equals(null)) || (iFrame.getItem().getType().equals(Material.AIR))) {
	      return;
	    }
	    if ((!player.isOp()) && (!player.hasPermission("frame.rotate")))
	    {
	      player.sendMessage(ChatColor.DARK_RED + msg3);
	      event.setCancelled(true);
	    }
	  }
	  
	  @EventHandler
	  public void ItemRemoval(EntityDamageByEntityEvent e)
	  {
	    String msg4 = PluginMeta.prefix + "Keine Rechte";
	    if ((e.getDamager() instanceof Player))
	    {
	      Player p = (Player)e.getDamager();
	      if ((e.getEntity().getType() == EntityType.ITEM_FRAME) && 
	        (!p.isOp()) && (!p.hasPermission("frame.item.remove")))
	      {
	        p.sendMessage(ChatColor.DARK_RED + msg4);
	        e.setCancelled(true);
	      }
	    }
	    if (((e.getDamager() instanceof Projectile)) && (e.getEntity().getType() == EntityType.ITEM_FRAME))
	    {
	      Projectile p = (Projectile)e.getDamager();
	      Player player = (Player)p.getShooter();
	      if ((!player.isOp()) && (!player.hasPermission("frame.item.remove")))
	      {
	        player.sendMessage(ChatColor.DARK_RED + msg4);
	        e.setCancelled(true);
	      }
	    }
	  }
	
}
