package net.pvp_hub.sg.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerJoinListener implements Listener {

	private SurvivalGames plugin;
	public PlayerJoinListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		
		e.setJoinMessage(null);
		PlayerUtilities.clearInventory(p);
		
		
		if(this.plugin.joinable){
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
				public void run(){
					Bukkit.broadcastMessage(p.getDisplayName() + " �6hat den Server betreten");
					
					ItemStack i = new ItemStack(Material.MAGMA_CREAM);
					ItemMeta iM = i.getItemMeta();
					iM.setDisplayName("�aMap-Voting");
					i.setItemMeta(iM);
					p.getInventory().addItem(i);
					
					p.setHealth(20.0);
					p.setFoodLevel(20);
					p.setFireTicks(0);
					p.setFlying(false);
					
					Location loc = new Location(Bukkit.getWorld("Lobby"), -1083, 24, -145);
					p.teleport(loc);
					
					int Tim = 0;
					Tim++; // #Tim++
					
					//p.setScoreboard(plugin.board);
				}
			});
			try {
				plugin.isInDatabase(p.getName());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			p.kickPlayer("Das Spiel hat bereits begonnen");
		}
	}
	
}
