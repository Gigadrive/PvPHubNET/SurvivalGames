package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class PlayerChangeFoodLevelListener implements Listener {

	private SurvivalGames plugin;
	public PlayerChangeFoodLevelListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onFood(FoodLevelChangeEvent e){
		if(!this.plugin.friendly){
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		}
	}
	
}
