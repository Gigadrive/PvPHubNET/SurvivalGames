package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

	private SurvivalGames plugin;
	public BlockBreakListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e){
		e.setCancelled(true);
	}
	
}
