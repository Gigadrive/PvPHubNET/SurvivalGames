package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {
	
	private SurvivalGames plugin;
	public EntityDamageByEntityListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e){
		if(this.plugin.friendly == true){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	
}
