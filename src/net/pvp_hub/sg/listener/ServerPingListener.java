package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

	private SurvivalGames plugin;
	public ServerPingListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPing(ServerListPingEvent e){
		String motdGameState = plugin.gamestate.replace("�", "\u00A7");
		
		e.setMotd(motdGameState);
	}
	
}
