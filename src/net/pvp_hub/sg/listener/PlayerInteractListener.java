package net.pvp_hub.sg.listener;

import java.util.Random;

import net.pvp_hub.sg.SurvivalGames;
import net.pvp_hub.sg.map.MapVote;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

	private SurvivalGames plugin;
	public PlayerInteractListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if(this.plugin.spectators.contains(p)){
				if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)){
					if(p.getItemInHand().getType() == Material.WATCH){
						Random rand = new Random();
						Player pl = (Player)this.plugin.online.get(rand.nextInt(this.plugin.online.size()));
						p.teleport(pl);
						p.sendMessage(plugin.prefix + "Du wurdest zu " + pl.getDisplayName() + " �6teleportiert.");
						
						World w = Bukkit.getWorld(plugin.cfg.getString("SG.Lobby.World"));
						double x = plugin.cfg.getDouble("SG.Lobby.X");
						double y = plugin.cfg.getDouble("SG.Lobby.Y");
						double z = plugin.cfg.getDouble("SG.Lobby.Z");
						float yaw = plugin.cfg.getInt("SG.Lobby.Yaw");
						float pitch = plugin.cfg.getInt("SG.Lobby.Pitch");
						
						Location loc = new Location(w, x, y, z);
						loc.setYaw(yaw);
						loc.setPitch(pitch);
						
						p.teleport(loc);
					}
				}
			}
			
			if(this.plugin.joinable){
				if((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)){
					if(p.getItemInHand().getItemMeta().getDisplayName().equals("�aMap-Voting")){
						p.openInventory(plugin.getMapVote().getInventory());
					}
				}
			}
		} catch(Exception e1){}
	}
	
}
