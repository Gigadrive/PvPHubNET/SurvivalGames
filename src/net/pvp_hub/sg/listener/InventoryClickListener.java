package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;
import net.pvp_hub.sg.map.MapVote;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

	private SurvivalGames plugin;
	public InventoryClickListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p;
		if(e.getWhoClicked() instanceof Player){
			p = (Player)e.getWhoClicked();
		} else {
			return;
		}
		
		try {
			if(e.getInventory().getName().equals("�4Map-Voting")){
				if(plugin.getMapVote().isValidMap(e.getCurrentItem())){
					if(plugin.getMapVote().hasUserVoted(p)){
						p.sendMessage(plugin.prefix + "�cDu hast bereits gevotet!");
						e.setCancelled(true);
						p.closeInventory();
					} else {
						plugin.getMapVote().addUserVote(p, e.getSlot());
						p.sendMessage(plugin.prefix + "�aDeine Stimme wurde gez�hlt!");
						p.closeInventory();
						e.setCancelled(true);
					}
				}
			}
		} catch(Exception e1){}
	}
	
}
