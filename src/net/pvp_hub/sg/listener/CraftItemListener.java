package net.pvp_hub.sg.listener;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;

public class CraftItemListener implements Listener {

	private SurvivalGames plugin;
	public CraftItemListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onCraft(CraftItemEvent e){
		if(e.getCurrentItem().getType() == Material.DIAMOND_SWORD){
			try {
				PlayerUtilities.achieve(10, (Player) e.getWhoClicked());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
}
