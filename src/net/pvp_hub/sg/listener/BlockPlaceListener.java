package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

public class BlockPlaceListener implements Listener {

	private SurvivalGames plugin;
	public BlockPlaceListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if(e.getBlock().getType() == Material.TNT){
			e.setCancelled(true);
			e.getPlayer().getInventory().removeItem(new ItemStack(Material.TNT));
			e.getPlayer().getWorld().spawnEntity(e.getBlock().getLocation().add(0.5D, 0.5D, 0.5D), EntityType.PRIMED_TNT);
		} else {
			e.setCancelled(true);
		}
	}
	
}
