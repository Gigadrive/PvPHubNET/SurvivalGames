package net.pvp_hub.sg.listener;

import java.io.IOException;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

	private SurvivalGames plugin;
	public PlayerDeathListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		
		e.setDeathMessage(null);
		
		if(p.getKiller() == null){
			
			try {
				plugin.removePoints(p.getName());
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			p.sendMessage(plugin.prefix + "�cDu hast �e5 �cPunkte verloren!");
			try {
				p.sendMessage(plugin.prefix + "�cDu hast jetzt noch �e" + SurvivalGames.getPoints(p.getName()) + " �cPunkte!");
			} catch (Exception e1) {

			}
			
			try {
				SurvivalGames.addDeaths(p.getName());
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			this.plugin.online.remove(p);
			this.plugin.spectators.add(p);
			
			p.sendMessage(PluginMeta.prefix + "�cDu bist ausgeschieden!");
			
			try {
				PlayerUtilities.connect(SurvivalGames.getInstance(), "lobby", p);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Bukkit.broadcastMessage(plugin.prefix + p.getDisplayName() + "�r �6ist gestorben!");
			Bukkit.broadcastMessage(plugin.prefix + "Es leben noch �b" + "" + plugin.online.size() + " �6Spieler");
			//Bukkit.broadcastMessage(plugin.prefix + "�7Spectator: �b" + plugin.spectators.size());
			
			if(plugin.online.size() == 1){
				Player last = plugin.online.get(0);
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�a�lDer Spieler " + last.getDisplayName() + " �a�lhat die SurvivalGames gewonnen!");
				Bukkit.broadcastMessage("");
				plugin.l = true;
				
				try {
					SurvivalGames.addWinPoints(last.getName());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				try {
					SurvivalGames.addVictory(last.getName());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				last.sendMessage(plugin.prefix + "�aDu hast �e50 �aPunkte bekommen!");
				
				try {
					PlayerUtilities.addCoins(last, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						for(Player all : Bukkit.getOnlinePlayers()){
							try {
								PlayerUtilities.connect(plugin, "lobby", all);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							plugin.gamestate = "Restarting";
							Bukkit.reload();
						}
					}
				}, 50L);
			}
		} else {
			Player killer = p.getKiller();
			
			if(plugin.firstblood == false){
				plugin.firstblood = true;
				Bukkit.broadcastMessage(plugin.prefix + "�4�lFirst Blood:�r " + killer.getDisplayName());
				try {
					PlayerUtilities.achieve(14, killer);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			try {
				SurvivalGames.addPoints(killer.getName());
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			try {
				SurvivalGames.addKills(killer.getName());
			} catch (Exception e4) {
				// TODO Auto-generated catch block
				e4.printStackTrace();
			}
			
			try {
				SurvivalGames.addDeaths(p.getName());
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			try {
				SurvivalGames.removePoints(p.getName());
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			p.sendMessage(plugin.prefix + "�cDu hast �e5 �cPunkte verloren!");
			killer.sendMessage(plugin.prefix + "�aDu hast �e5 �aPunkte f�r's T�ten von " + p.getDisplayName() + " �aerhalten!");
			try {
				p.sendMessage(plugin.prefix + "�cDu hast jetzt noch �e" + SurvivalGames.getPoints(p.getName()) + " �cPunkte!");
			} catch (Exception e1) {

			}
			
			try {
				p.getKiller().sendMessage(plugin.prefix + "�aDu hast jetzt �e" + SurvivalGames.getPoints(p.getKiller().getName()) + " �aPunkte!");
			} catch (Exception e1) {

			}
			
			
			
			try {
				PlayerUtilities.addCoins(killer, 5);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			this.plugin.online.remove(p);
			this.plugin.spectators.add(p);
			
			p.sendMessage(PluginMeta.prefix + "�cDu bist ausgeschieden!");
			
			try {
				PlayerUtilities.connect(SurvivalGames.getInstance(), "lobby", p);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			Bukkit.broadcastMessage(plugin.prefix + p.getDisplayName() + "�r �6wurde von " + killer.getDisplayName() + " �r�6get�tet!");
			Bukkit.broadcastMessage(plugin.prefix + "Es leben noch �b" + "" + plugin.online.size() + " �6Spieler");
			//Bukkit.broadcastMessage(plugin.prefix + "�7Spectator: �b" + plugin.spectators.size());
			
			if(this.plugin.online.size() == 1){
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�a�lDer Spieler " + killer.getDisplayName() + " �a�lhat die SurvivalGames gewonnen!");
				Bukkit.broadcastMessage("");
				PvPHubCore.setGameState(GameState.ENDING);
				plugin.l = true;
				
				try {
					SurvivalGames.addVictory(killer.getName());
				} catch (Exception e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				
				try {
					if(SurvivalGames.getVictories(killer.getName()) == 1){
						try {
							PlayerUtilities.achieve(11, killer);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				} catch (Exception e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				
				try {
					if(SurvivalGames.getVictories(killer.getName()) == 20){
						try {
							PlayerUtilities.achieve(12, killer);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				} catch (Exception e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				
				try {
					if(SurvivalGames.getVictories(killer.getName()) == 50){
						try {
							PlayerUtilities.achieve(13, killer);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				} catch (Exception e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
				}
				
				try {
					SurvivalGames.addWinPoints(killer.getName());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				killer.sendMessage(plugin.prefix + "�aDu hast �e50 �aPunkte bekommen!");
				
				try {
					PlayerUtilities.addCoins(killer, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						for(Player all : Bukkit.getOnlinePlayers()){
							try {
								PlayerUtilities.connect(SurvivalGames.getInstance(), "lobby", all);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							plugin.gamestate = "Restarting";
						
							Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
								@Override
								public void run() {
									Bukkit.reload();
								}
							}, 50L);
						}
					}
				}, 50L);
			}
			
		}
			
	}
	
}
