package net.pvp_hub.sg.listener;

import java.io.IOException;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

	private SurvivalGames plugin;
	public PlayerQuitListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		e.setQuitMessage(e.getPlayer().getDisplayName() + " �6hat den Server verlassen.");
		plugin.online.remove(e.getPlayer());
		
		if(plugin.joinable == false){
			try {
				plugin.removePoints(p.getName());
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			p.sendMessage(plugin.prefix + "�cDu hast �e5 �cPunkte verloren!");
			try {
				p.sendMessage(plugin.prefix + "�cDu hast jetzt noch �e" + SurvivalGames.getPoints(p.getName()) + " �cPunkte!");
			} catch (Exception e1) {

			}
			
			try {
				SurvivalGames.addDeaths(p.getName());
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			this.plugin.online.remove(p);
			this.plugin.spectators.add(p);
			
			p.sendMessage(PluginMeta.prefix + "�cDu bist ausgeschieden!");
			
			try {
				PlayerUtilities.connect(SurvivalGames.getInstance(), "lobby", p);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Bukkit.broadcastMessage(plugin.prefix + p.getDisplayName() + "�r �6ist gestorben!");
			Bukkit.broadcastMessage(plugin.prefix + "Es leben noch �b" + "" + plugin.online.size() + " �6Spieler");
			//Bukkit.broadcastMessage(plugin.prefix + "�7Spectator: �b" + plugin.spectators.size());
			
			if(plugin.online.size() == 1 && plugin.l == false){
				Player last = plugin.online.get(0);
				Bukkit.broadcastMessage("");
				Bukkit.broadcastMessage("�a�lDer Spieler " + last.getDisplayName() + " �a�lhat die SurvivalGames gewonnen!");
				Bukkit.broadcastMessage("");
				
				try {
					SurvivalGames.addWinPoints(last.getName());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				try {
					SurvivalGames.addVictory(last.getName());
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				last.sendMessage(plugin.prefix + "�aDu hast �e50 �aPunkte bekommen!");
				
				try {
					PlayerUtilities.addCoins(last, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						for(Player all : Bukkit.getOnlinePlayers()){
							try {
								PlayerUtilities.connect(plugin, "lobby", all);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							plugin.gamestate = "Restarting";
							Bukkit.reload();
						}
					}
				}, 50L);
			}
		}
	}
	
}
