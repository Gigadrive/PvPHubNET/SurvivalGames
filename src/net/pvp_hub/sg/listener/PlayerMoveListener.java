package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

	private SurvivalGames plugin;
	public PlayerMoveListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(this.plugin.onspawn){
			Location loc = plugin.spawns.get(p);
			p.teleport(loc);
		}
	}
	
}
