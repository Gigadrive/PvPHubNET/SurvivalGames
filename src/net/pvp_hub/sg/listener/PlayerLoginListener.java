package net.pvp_hub.sg.listener;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class PlayerLoginListener implements Listener {

	private SurvivalGames plugin;
	public PlayerLoginListener(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onLogin(PlayerLoginEvent e){
		Player p = e.getPlayer();
		
		if(!plugin.joinable){
			e.setResult(Result.KICK_OTHER);
			e.setKickMessage("Das Spiel hat bereits begonnen.");
		}
	}
	
}
