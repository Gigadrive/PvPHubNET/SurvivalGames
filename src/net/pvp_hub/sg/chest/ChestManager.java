package net.pvp_hub.sg.chest;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ChestManager implements Listener {

	private SurvivalGames plugin;
	public ChestManager(SurvivalGames plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		try {
			if((e.getAction() == Action.RIGHT_CLICK_BLOCK) && (e.getClickedBlock().getType() == Material.REDSTONE_BLOCK)) {
				if(this.plugin.sgchest.containsKey(e.getClickedBlock().getLocation())){
					p.openInventory((Inventory)this.plugin.sgchest.get(e.getClickedBlock().getLocation()));
				} else {
					Random rnd = new Random();
					int n = 1;
					n = rnd.nextInt(6);
					Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
					List<ItemStack> items = new ArrayList<ItemStack>();
					
					// Level 1 (10x)
					for(int i = 1; i <= 51; i++) {
						items.add(new ItemStack(Material.WOOD_AXE));
						items.add(new ItemStack(Material.STONE_AXE));
						items.add(new ItemStack(Material.LEATHER_HELMET));
						items.add(new ItemStack(Material.LEATHER_CHESTPLATE));
						items.add(new ItemStack(Material.LEATHER_LEGGINGS));
						items.add(new ItemStack(Material.LEATHER_BOOTS));
						items.add(new ItemStack(Material.ROTTEN_FLESH));
						items.add(new ItemStack(Material.CARROT));
						items.add(new ItemStack(Material.POTATO));
						items.add(new ItemStack(Material.FISHING_ROD));
						items.add(new ItemStack(Material.SNOW_BALL));
						items.add(new ItemStack(Material.PORK));
						items.add(new ItemStack(Material.GOLD_SWORD));
						items.add(new ItemStack(Material.RAW_FISH));
					}
					
					// Level 2 (20x)
					for(int i = 1; i <= 41; i++) {
						items.add(new ItemStack(Material.WOOD_SWORD));
						items.add(new ItemStack(Material.GOLD_HELMET));
						items.add(new ItemStack(Material.GOLD_CHESTPLATE));
						items.add(new ItemStack(Material.GOLD_LEGGINGS));
						items.add(new ItemStack(Material.GOLD_BOOTS));
						items.add(new ItemStack(Material.FIREBALL));
						items.add(new ItemStack(Material.ARROW, 16));
						items.add(new ItemStack(Material.BREAD));
						items.add(new ItemStack(Material.FLINT));
					}
					
					// Level 3 (30x)
					for(int i = 1; i <= 31; i++) {
						items.add(new ItemStack(Material.STONE_SWORD));
						items.add(new ItemStack(Material.TNT));
						items.add(new ItemStack(Material.CHAINMAIL_HELMET));
						items.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
						items.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
						items.add(new ItemStack(Material.CHAINMAIL_BOOTS));
						items.add(new ItemStack(Material.COOKED_BEEF));
						items.add(new ItemStack(Material.BOW));
						items.add(new ItemStack(Material.IRON_INGOT));
					}
					
					// Level 4 (40x)
					for(int i = 1; i <= 21; i++) {
						items.add(new ItemStack(Material.IRON_SWORD));
						items.add(new ItemStack(Material.GOLDEN_APPLE));
						items.add(new ItemStack(Material.IRON_HELMET));
						items.add(new ItemStack(Material.IRON_CHESTPLATE));
						items.add(new ItemStack(Material.IRON_LEGGINGS));
						items.add(new ItemStack(Material.IRON_BOOTS));
						items.add(new ItemStack(Material.EXP_BOTTLE));
						items.add(new ItemStack(Material.FLINT_AND_STEEL));
						items.add(new ItemStack(Material.STICK));
					}
					
					// Level 5 (50x)
					for(int i = 1; i <= 6; i++) {
						items.add(new ItemStack(Material.DIAMOND));
					}
					
					while(n != 0){
						n--;
						Random rnd2 = new Random();
						Random rnd3 = new Random();
						
						int n3 = rnd3.nextInt(27);
						
						int n2 = rnd2.nextInt(items.size());
						
						inv.setItem(n3, (ItemStack)items.get(n2));
					}
					
					plugin.sgchest.put(e.getClickedBlock().getLocation(), inv);
					p.openInventory(inv);
					return;
				}
			}
		} catch(Exception e1){}
	}
	
}
