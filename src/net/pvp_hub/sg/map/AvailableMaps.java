package net.pvp_hub.sg.map;

public enum AvailableMaps {

	RISE_ORIENT(0, "Rise of the Orient", "Team OPalien", "http://opalien.de"),
	SG4(1, "Survival Games 4", "Team Vareide", "http://youtube.com/vareide"),
	VENEZIA(2, "Venezia", "Discovery Works", "http://www.planetminecraft.com/member/discovery_works/");
	
	private int id;
	private String name;
	private String builder;
	private String link;
	
	AvailableMaps(int id, String name, String builder, String link){
		this.id = id;
		this.name = name;
		this.builder = builder;
		this.link = link;
	}
	
	public int getID(){
		return this.id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getBuilder(){
		return this.builder;
	}
	
	public String getLink(){
		return this.link;
	}
	
	public static AvailableMaps fromID(int id){
		switch (id){
			case 0: return RISE_ORIENT;
			case 1: return SG4;
			case 2: return VENEZIA;
		}
		return null;
	}
	
}
