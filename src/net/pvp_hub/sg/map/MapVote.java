package net.pvp_hub.sg.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MapVote {

	private SurvivalGames plugin;
	private Inventory inv;
	private ItemStack Map1;
	private ItemStack Map2;
	private ItemStack Map3;
	private HashMap<Player, Integer> VoteBox;
	
	private List<String> descriptionMap1 = new ArrayList<String>();
	private List<String> descriptionMap2 = new ArrayList<String>();
	private List<String> descriptionMap3 = new ArrayList<String>();
	
	public MapVote(SurvivalGames plugin){
		this.plugin = plugin;
		initInventories();
		init();
	}
	
	public void addUserVote(Player p, int MapID){
		VoteBox.put(p, MapID);
	}
	
	public SurvivalGames getSG(){
		return this.plugin;
	}
	
	public static String getWorldNameByID(int id) {
		switch(id){
			case 0:
				return "hg_orient";
			case 1:
				return "sg4";
			case 2:
				return "dw_venezia";
			default:
				return "";
		}
	}
	
	public Inventory getInventory(){
		return this.inv;
	}
	
	public ItemStack getMap1Object(){
		return this.Map1;
	}
	
	public ItemStack getMap2Object(){
		return this.Map2;
	}
	
	public ItemStack getMap3Object(){
		return this.Map3;
	}
	
	public int getWorldWithMostVotes(){
		if(VoteBox.size() == 0){
			return 0;
		} else {
			Entry<Player, Integer> maxEntry = null;
			
			for(Entry<Player, Integer> entry : VoteBox.entrySet()){
				if(maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0){
					maxEntry = entry;
				}
			}
			return maxEntry.getValue();
		}
	}
	
	public boolean hasUserVoted(Player p){
		if(VoteBox.containsKey(p)){
			return true;
		} else {
			return false;
		}
	}
	
	private void init(){
		VoteBox = new HashMap<Player, Integer>();
	}
	
	public boolean isValidMap(ItemStack i) {
		if (i.equals(Map1) || i.equals(Map2) || i.equals(Map3)) {
			return true;
		} else {
			return false;
		}
	}
	
	private void initInventories(){
		
		if(inv == null) inv = plugin.getServer().createInventory(null, 18, "�4Map-Voting");
		
			ItemStack Map1 = new ItemStack(Material.EMPTY_MAP, 1);
			ItemStack Map2 = new ItemStack(Material.EMPTY_MAP, 1);
			ItemStack Map3 = new ItemStack(Material.EMPTY_MAP, 1);
			ItemMeta Map1Meta = Map1.getItemMeta();
			ItemMeta Map2Meta = Map2.getItemMeta();
			ItemMeta Map3Meta = Map3.getItemMeta();
			
			Map1Meta.setDisplayName("�9" + AvailableMaps.RISE_ORIENT.getName());
			Map2Meta.setDisplayName("�9" + AvailableMaps.SG4.getName());
			Map3Meta.setDisplayName("�9" + AvailableMaps.VENEZIA.getName());
			
			descriptionMap1.add("�3Erbauer: �7" + AvailableMaps.RISE_ORIENT.getBuilder());
			descriptionMap1.add("�3Link: �7" + AvailableMaps.RISE_ORIENT.getLink());
			
			descriptionMap2.add("�3Erbauer: �7" + AvailableMaps.SG4.getBuilder());
			descriptionMap2.add("�3Link: �7" + AvailableMaps.SG4.getLink());
			
			descriptionMap3.add("�3Erbauer: �7" + AvailableMaps.VENEZIA.getBuilder());
			descriptionMap3.add("�3Link: �7" + AvailableMaps.VENEZIA.getLink());
			
			Map1Meta.setLore(descriptionMap1);
			Map2Meta.setLore(descriptionMap2);
			Map3Meta.setLore(descriptionMap3);
			
			Map1.setItemMeta(Map1Meta);
			Map2.setItemMeta(Map2Meta);
			Map3.setItemMeta(Map3Meta);
			
			ItemStack[] it = new ItemStack[18];
			it[0] = Map1;
			it[1] = Map2;
			it[2] = Map3;
			
			this.Map1 = Map1;
			this.Map2 = Map2;
			this.Map3 = Map3;
			inv.setContents(it);
	}
	
}
