package net.pvp_hub.sg.map;

import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;

public class Map {

	private int id;
	private World world;
	private Location[] Spawn;
	private SurvivalGames plugin;
	
	public Map(SurvivalGames plugin, int Map){
		this.id = Map;
		this.plugin = plugin;
		initArray(Map);
		initWorld(Map);
	}
	
	private void initArray(int id){
		if(AvailableMaps.fromID(id) == AvailableMaps.RISE_ORIENT){
			Spawn = new Location[24];
			Spawn[0] = new Location(world, 0.47535906201869194, 26.0, -18.7875276473693);
			Spawn[1] = new Location(world, 5.527044808188917, 26.0, -18.484528072945743);
			Spawn[2] = new Location(world, 10.704422339877341, 26.0, -16.284838898973128);
			Spawn[3] = new Location(world, 14.586888892147899, 26.0, -13.459577937119224);
			Spawn[4] = new Location(world, 17.604107658825562, 26.0, -9.359725686076434);
			Spawn[5] = new Location(world, 19.646119376043806, 26.0, -4.609621097722689);
			Spawn[6] = new Location(world, 19.646119376043806, 26.0, 0.5552603561178762);
			Spawn[7] = new Location(world, 19.646119376043806, 26.0, 5.276079024537021);
			Spawn[8] = new Location(world, 17.84186702022795, 26.0, 10.687093803759932);
			Spawn[9] = new Location(world, 14.866975835057945, 26.0, 14.644973877568598);
			Spawn[10] = new Location(world, 10.377188362105846, 26.0, 17.543239198174756);
			Spawn[11] = new Location(world, 5.424417738210235, 26.0, 19.684920383000748);
			Spawn[12] = new Location(world, 0.49020207938450294, 26.0, 20.31015775466424);
			Spawn[13] = new Location(world, -4.675111227199121, 26.0, 19.557460079122063);
			Spawn[14] = new Location(world, -9.703105206833865, 26.0, 17.57422136505951);
			Spawn[15] = new Location(world, -13.691263083154762, 26.0, 14.812709158623452);
			Spawn[16] = new Location(world, -16.69461973457635, 26.0, 10.455237522163994);
			Spawn[17] = new Location(world, -18.3410075964429, 26.0, 5.544381353982739);
			Spawn[18] = new Location(world, -18.54837012731609, 26.0, 0.41268433248500125);
			Spawn[19] = new Location(world, -18.44123393336572, 26.0, -4.541380967305946);
			Spawn[20] = new Location(world, -16.66013467483547, 26.0, -9.669850495713838);
			Spawn[21] = new Location(world, -13.423676164896081, 26.0, -13.814327743259911);
			Spawn[22] = new Location(world, -9.249464570625484, 26.0, -16.520609414907536);
			Spawn[23] = new Location(world, -4.533735542059581, 26.0, -18.61477587618474);
		} else if(AvailableMaps.fromID(id) == AvailableMaps.SG4){
			Spawn = new Location[24];
			Spawn[0] = new Location(world, 0.522, 30.0, -19.545);
			Spawn[1] = new Location(world, 4.436, 30.0, -19.261);
			Spawn[2] = new Location(world, 8.576, 30.0, -17.482);
			Spawn[3] = new Location(world, 12.396, 30.0, -14.395);
			Spawn[4] = new Location(world, 15.472, 30.0, -10.520);
			Spawn[5] = new Location(world, 17.457, 30.0, -6.542);
			Spawn[6] = new Location(world, 17.261, 30.0, -2.427);
			Spawn[7] = new Location(world, 17.478, 30.0, 1.668);
			Spawn[8] = new Location(world, 15.477, 30.0, 5.468);
			Spawn[9] = new Location(world, 12.329, 30.0, 9.559);
			Spawn[10] = new Location(world, 8.511, 30.0, 12.495);
			Spawn[11] = new Location(world, 4.503, 30.0, 14.485);
			Spawn[12] = new Location(world, 0.490, 30.0, 14.633);
			Spawn[13] = new Location(world, -3.455, 30.0, 14.453);
			Spawn[14] = new Location(world, -7.495, 30.0, 12.621);
			Spawn[15] = new Location(world, -11.536, 30.0, 9.452);
			Spawn[16] = new Location(world, -14.454, 30.0, 5.475);
			Spawn[17] = new Location(world, -16.651, 30.0, 1.570);
			Spawn[18] = new Location(world, -16.651, 30.0, -2.562);
			Spawn[19] = new Location(world, -16.651, 30.0, -6.425);
			Spawn[20] = new Location(world, -14.299, 30.0, -10.652);
			Spawn[21] = new Location(world, -11.757, 30.0, -14.487);
			Spawn[22] = new Location(world, -7.683, 30.0, -17.625);
			Spawn[23] = new Location(world, -3.602, 30.0, -19.618);
		} else if(AvailableMaps.fromID(id) == AvailableMaps.VENEZIA){
			Spawn = new Location[24];
			Spawn[0] = new Location(world, -917.456, 13.0, 337.496);
			Spawn[1] = new Location(world, -911.578, 13.0, 338.368);
			Spawn[2] = new Location(world, -904.494, 13.0, 341.375);
			Spawn[3] = new Location(world, -900.498, 13.0, 344.462);
			Spawn[4] = new Location(world, -897.374, 13.0, 348.481);
			Spawn[5] = new Location(world, -894.425, 13.0, 355.427);
			Spawn[6] = new Location(world, -893.741, 13.0, 361.506);
			Spawn[7] = new Location(world, -894.725, 13.0, 367.467);
			Spawn[8] = new Location(world, -896.428, 13.0, 373.528);
			Spawn[9] = new Location(world, -900.684, 13.0, 378.691);
			Spawn[10] = new Location(world, -905.417, 13.0, 382.408);
			Spawn[11] = new Location(world, -911.441, 13.0, 384.481);
			Spawn[12] = new Location(world, -917.435, 13.0, 385.551);
			Spawn[13] = new Location(world, -923.587, 13.0, 384.520);
			Spawn[14] = new Location(world, -929.636, 13.0, 382.643);
			Spawn[15] = new Location(world, -934.732, 13.0, 378.502);
			Spawn[16] = new Location(world, -937.464, 13.0, 374.559);
			Spawn[17] = new Location(world, -940.610, 13.0, 369.369);
			Spawn[18] = new Location(world, -941.520, 13.0, 361.467);
			Spawn[19] = new Location(world, -940.583, 13.0, 353.594);
			Spawn[20] = new Location(world, -937.683, 13.0, 348.364);
			Spawn[21] = new Location(world, -934.654, 13.0, 344.408);
			Spawn[22] = new Location(world, -930.508, 13.0, 341.442);
			Spawn[23] = new Location(world, -923.504, 13.0, 338.502);
		}
	}
	
	private void initWorld(int id){
		if(AvailableMaps.fromID(id) == AvailableMaps.RISE_ORIENT){
			this.world = plugin.getServer().getWorld("hg_orient");
		} else if(AvailableMaps.fromID(id) == AvailableMaps.SG4){
			this.world = plugin.getServer().getWorld("sg4");
		} else if(AvailableMaps.fromID(id) == AvailableMaps.VENEZIA){
			this.world = plugin.getServer().getWorld("dw_venezia");
		} else {
			this.world = null;
		}
	}
	
	public World getWorld(){
		return this.world;
	}
	
	public Location getSpawnLocation(int id){
		return Spawn[id];
	}
	
}
