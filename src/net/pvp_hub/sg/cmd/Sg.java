package net.pvp_hub.sg.cmd;

import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Sg implements CommandExecutor {

	private SurvivalGames plugin;
	public Sg(SurvivalGames plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("sg")){
			if((sender instanceof Player)){
				Player p = (Player)sender;
				if(args.length == 0){
					p.sendMessage(plugin.prefix + "");
				}
				
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("start")){
						try {
							if(net.pvp_hub.core.api.PlayerUtilities.getRank(p.getName()) > 6){
								plugin.dings = 11;
							} else {
								p.sendMessage(PluginMeta.noperms);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					if(args[0].equalsIgnoreCase("setlobby")){
						try {
							if(net.pvp_hub.core.api.PlayerUtilities.getRank(p.getName()) > 9){
								String w = p.getWorld().getName();
								double x = p.getLocation().getX();
								double y = p.getLocation().getY();
								double z = p.getLocation().getZ();
								double yaw = p.getLocation().getYaw();
								double pitch = p.getLocation().getPitch();
								
								p.sendMessage(plugin.prefix + "�aWorld: �e" + w);
								p.sendMessage(plugin.prefix + "�aX: �e" + x);
								p.sendMessage(plugin.prefix + "�aY: �e" + y);
								p.sendMessage(plugin.prefix + "�aZ: �e" + z);
								p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
								p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
								
								plugin.cfg.set("SG.Lobby.World", w);
								plugin.cfg.set("SG.Lobby.X", x);
								plugin.cfg.set("SG.Lobby.Y", y);
								plugin.cfg.set("SG.Lobby.Z", z);
								plugin.cfg.set("SG.Lobby.Yaw", yaw);
								plugin.cfg.set("SG.Lobby.Pitch", pitch);
								plugin.saveConfig();
								plugin.cfg.save(plugin.file);
							} else {
								p.sendMessage(PluginMeta.noperms);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else if(args.length == 2){
					if(args[0].equalsIgnoreCase("setspawn")){
						try {
							if(net.pvp_hub.core.api.PlayerUtilities.getRank(p.getName()) > 9){
								String w = p.getWorld().getName();
								double x = p.getLocation().getX();
								double y = p.getLocation().getY();
								double z = p.getLocation().getZ();
								double yaw = p.getLocation().getYaw();
								double pitch = p.getLocation().getPitch();
								
								p.sendMessage(plugin.prefix + "�aWorld: �e" + w);
								p.sendMessage(plugin.prefix + "�aX: �e" + x);
								p.sendMessage(plugin.prefix + "�aY: �e" + y);
								p.sendMessage(plugin.prefix + "�aZ: �e" + z);
								p.sendMessage(plugin.prefix + "�aYaw: �e" + yaw);
								p.sendMessage(plugin.prefix + "�aPitch: �e" + pitch);
								
								plugin.cfg.set("SG.Spawn." + args[1] + ".World", w);
								plugin.cfg.set("SG.Spawn." + args[1] + ".x", x);
								plugin.cfg.set("SG.Spawn." + args[1] + ".y", y);
								plugin.cfg.set("SG.Spawn." + args[1] + ".z", z);
								plugin.cfg.set("SG.Spawn." + args[1] + ".yaw", yaw);
								plugin.cfg.set("SG.Spawn." + args[1] + ".pitch", pitch);
								plugin.saveConfig();
								plugin.cfg.save(plugin.file);
							} else {
								p.sendMessage(PluginMeta.noperms);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return false;
	}
	
	
	
}
