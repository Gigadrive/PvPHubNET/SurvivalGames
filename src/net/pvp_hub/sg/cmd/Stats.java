package net.pvp_hub.sg.cmd;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;
import net.pvp_hub.sg.SurvivalGames;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats implements CommandExecutor {

	private SurvivalGames plugin;
	public Stats(SurvivalGames plugin){
		this.plugin = plugin;
	}
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		final Player p = (Player)sender;
		
		if(cmd.getName().equalsIgnoreCase("stats")){
			if(args.length == 0){
				if((sender instanceof Player)){
					Player pl = (Player)sender;
					String pn = pl.getName();
					
					try {
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						sender.sendMessage(SurvivalGames.prefix + "�6Points: �e" + SurvivalGames.getPoints(pn));
						sender.sendMessage(SurvivalGames.prefix + "�6Kills: �e" + SurvivalGames.getKills(pn));
						sender.sendMessage(SurvivalGames.prefix + "�6Deaths: �e" + SurvivalGames.getDeaths(pn));
						sender.sendMessage(SurvivalGames.prefix + "�6Gewonnene Spiele: �e" + SurvivalGames.getVictories(pn));
						sender.sendMessage(SurvivalGames.prefix + "�6Gespielte Spiele: �e" + SurvivalGames.getPlayedGames(pn));
						sender.sendMessage(SurvivalGames.prefix + "�6Showdowns: �e" + SurvivalGames.getShowdowns(pn));
						//sender.sendMessage(SurvivalGames.prefix + "�6K/D: �e" + SurvivalGames.getKD(pn));
						//sender.sendMessage(SurvivalGames.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					sender.sendMessage(PluginMeta.noplayer);
				}
			} else {
				if(PlayerUtilities.isOnline(args[0])){
					Player pl = Bukkit.getServer().getPlayer(args[0]);
					
					try {
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						sender.sendMessage(SurvivalGames.prefix + "�6Points: �e" + SurvivalGames.getPoints(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Kills: �e" + SurvivalGames.getKills(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Deaths: �e" + SurvivalGames.getDeaths(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Gewonnene Spiele: �e" + SurvivalGames.getVictories(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Gespielte Spiele: �e" + SurvivalGames.getPlayedGames(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Showdowns: �e" + SurvivalGames.getShowdowns(pl.getName()));
						//sender.sendMessage(SurvivalGames.prefix + "�6K/D: �e" + SurvivalGames.getKD(pl.getName()));
						//sender.sendMessage(SurvivalGames.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					OfflinePlayer pl = Bukkit.getServer().getOfflinePlayer(args[0]);
					try {
						String col = "�7";
						int r = PlayerUtilities.getRank(pl.getName());
						
						if(r == 0){
							col = "�7";
						} else if(r == 1){
							col = "�6";
						} else if(r == 2){
							col = "�6";
						} else if(r == 3){
							col = "�5";
						} else if(r == 4){
							col = "�5";
						} else if(r == 5){
							col = "�b";
						} else if(r == 6){
							col = "�3";
						} else if(r == 7){
							col = "�a";
						} else if(r == 8){
							col = "�e";
						} else if(r == 9){
							col = "�9";
						} else if(r == 10){
							col = "�c";
						} else if(r == 11){
							col = "�c";
						}
						
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
						sender.sendMessage(SurvivalGames.prefix + "�6Punkte: �e" + SurvivalGames.getPoints(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Kills: �e" + SurvivalGames.getKills(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Deaths: �e" + SurvivalGames.getDeaths(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Gewonnene Spiele: �e" + SurvivalGames.getVictories(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Gespielte Spiele: �e" + SurvivalGames.getPlayedGames(pl.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�6Showdowns: �e" + SurvivalGames.getShowdowns(pl.getName()));
						//sender.sendMessage(SurvivalGames.prefix + "�6K/D: �e" + SurvivalGames.getKD(pl.getName()));
						//sender.sendMessage(Vars.prefix + "�6H�chster Killstreak: �e" + SurvivalGames.getHighestStreak(p.getName()));
						sender.sendMessage(SurvivalGames.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		
		return false;
	}
	
}
